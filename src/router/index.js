import Vue from 'vue'
import VueRouter from 'vue-router'

// Pages
import Home from '../views/Home'
import ChartPage from '../views/ChartPage'

Vue.use(VueRouter)

// Routes
const router = new VueRouter({
  mode: 'history',
  linkActiveClass: 'is-active',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/grafico',
      name: 'Chart',
      component: ChartPage
    },
    { 
      path: '*', 
      redirect: '/'
    },
  ],
});

export default router
