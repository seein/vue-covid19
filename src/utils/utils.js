const months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
const weekdays = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];

// Devuelve fecha en formato: 'Sábado, 26 de Junio'
export const formatDate = (date) => `${ weekdays[date.getDay()] }, ${ date.getDate() } de ${ months[date.getMonth()] }`;