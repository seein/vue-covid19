import axios from 'axios'

const BASE_URL = 'https://api.covid19api.com'

// Fecha inicial y final
let fromDate = new Date(new Date().setDate(new Date().getDate() - 21)).toLocaleString('es-CL').split(' ')[0].split('-')
fromDate = `${ fromDate[2] }-${ fromDate[1] }-${ fromDate[0] }`

let toDate = new Date(new Date().setDate(new Date().getDate() - 1)).toLocaleString('es-CL').split(' ')[0].split('-')
toDate = `${ toDate[2] }-${ toDate[1] }-${ toDate[0] }`

export const getSummary = () => {
  const url = `${ BASE_URL }/summary`
  return axios.get(url)
}

export const getCountries = () => {
  const url = `${ BASE_URL }/countries`
  return axios.get(url)
}

export const getDataByCountryAndStatus = (country, status) => {
  const url = `${ BASE_URL }/country/${ country }/status/${ status }?from=${ fromDate }&to=${ toDate }`
  return axios.get(url)
}