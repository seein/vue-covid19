# Datos COVID-19 - Vue.js

<img src="/src/assets/images/imagen1-min.jpg?raw=true" width="45%">
<img src="/src/assets/images/imagen2-min.jpg?raw=true" width="45%">


### Pre-requisitos 📋

* Node.js - npm

### Instalación 🔧

_En el directorio del proyecto ingresar el siguiente comando a través de una terminal_

* npm install

## Despliegue 📦

_Ejecutar el siguiente comando para correr el proyecto_

* npm run serve

_Cuando termine, ingresar a http://localhost:8080_

## Construido con 🛠️

* [Vue.js](https://vuejs.org/v2/guide/)
* [Vuetify](https://vuetifyjs.com/en/getting-started/installation/)
* [API-COVID](https://documenter.getpostman.com/view/10808728/SzS8rjbc#81415d42-eb53-4a85-8484-42d2349debfe)